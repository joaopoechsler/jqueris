<?
	$cores = array('red','blue','violet','green','orange','purple','silver','brown','lime','magenta');

	$estados = array( // Graças ao google os programadores preguiçosos não precisam estudar geografia, http://snipplr.com/view/27496/array-de-estados-brasileiros/
		"AC" => "Acre", 
		"AL" => "Alagoas", 
		"AM" => "Amazonas", 
		"AP" => "Amapá",
		"BA" => "Bahia",
		"CE" => "Ceará",
		"DF" => "Distrito Federal",
		"ES" => "Espírito Santo",
		"GO" => "Goiás",
		"MA" => "Maranhão",
		"MT" => "Mato Grosso",
		"MS" => "Mato Grosso do Sul",
		"MG" => "Minas Gerais",
		"PA" => "Pará",
		"PB" => "Paraíba",
		"PR" => "Paraná",
		"PE" => "Pernambuco",
		"PI" => "Piauí",
		"RJ" => "Rio de Janeiro",
		"RN" => "Rio Grande do Norte",
		"RO" => "Rondônia",
		"RS" => "Rio Grande do Sul",
		"RR" => "Roraima",
		"SC" => "Santa Catarina",
		"SE" => "Sergipe",
		"SP" => "São Paulo",
		"TO" => "Tocantins"
	);

	// ESTADOS DE ACORDO COM A API
	$estadosAPI = array(
		12 => "AC",
		27 => "AL",
		13 => "AM",
		16 => "AP",
		29 => "BA",
		23 => "CE",
		53 => "DF",
		32 => "ES",
		52 => "GO",
		21 => "MA",
		51 => "MT",
		50 => "MS",
		31 => "MG",
		15 => "PA",
		25 => "PB",
		41 => "PR",
		26 => "PE",
		22 => "PI",
		33 => "RJ",
		24 => "RN",
		11 => "RO",
		43 => "RS",
		14 => "RR",
		42 => "SC",
		28 => "SE",
		35 => "SP",
		17 => "TO"
	);

	$meses = array(
		'01'=>'Janeiro',
		'02'=>'Fevereiro',
		'03'=>'Março',
		'04'=>'Abril',
		'05'=>'Maio',
		'06'=>'Junho',
		'07'=>'Julho',
		'08'=>'Agosto',
		'09'=>'Setembro',
		'10'=>'Outubro',
		'11'=>'Novembro',
		'12'=>'Dezembro'
	);
	$meses2 = array(
		'01'=>'JAN',
		'02'=>'FEV',
		'03'=>'MAR',
		'04'=>'ABR',
		'05'=>'MAI',
		'06'=>'JUN',
		'07'=>'JUL',
		'08'=>'AGO',
		'09'=>'SET',
		'10'=>'OUT',
		'11'=>'NOV',
		'12'=>'DEZ'
	);
	$dias = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
	$diasShort = array('Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb');

?>