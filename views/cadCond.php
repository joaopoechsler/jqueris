<center>
    <h2>Cadastro de Condominios</h2>
    <form class="col col-6" action="http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/register-condo">

        <select name="from_administradora" class="fromAdministradora col-12 custom-select mt-2">
            
        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="nome" value="" placeholder="Nome" required>
        <input class="col col-12 mt-2 form-control" type="text" name="qtblocos" value="" placeholder="Quantidade de Blocos" required>
        <input class="col col-12 mt-2 form-control" type="text" name="rua" value="" placeholder="Rua" required>
        <input class="col col-12 mt-2 form-control" type="text" name="num" value="" placeholder="Número" required>
        <input class="col col-12 mt-2 form-control" type="text" name="bairro" value="" placeholder="Bairro" required>
        <input class="col col-12 mt-2 form-control" type="text" name="cep" value="" placeholder="Bairro" required>

        <select name="estado" class="fromEstado col-12 custom-select mt-2">
            <option value="">Selecione o Estado</option>
            <? foreach ($estadosAPI as $sig => $uf) { ?>
                <option value="<?=$uf?>" data-uf=<?=$sig?>><?= $uf ?></option>
            <? } ?>
        </select>

        <select name="cidade" class="cidades col-12 custom-select mt-2">

        </select>

        <button class="btn btn-primary text-center my-2" type="submit">Enviar</button>
    </form>
</center>