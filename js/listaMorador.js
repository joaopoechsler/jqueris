$(function () {

    function renderizaMorador() {
        $('#renderMorador').html('');
        estrutura = `
            <tr>
            <td>Nome</td>
            <td>Condominio</td>
            <td>Bloco</td>
            <td>Metragem</td>
            <td>qtVagas</td>
            <td>Data Cadastro</td>
            </tr>
        `;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/unidades/get-all',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.resultSet) {
                    for (let i = 0; i < data.resultSet.length; i++) {
                        estrutura += `
                    <tr data-id="${data.resultSet[i].id}">
                        <td class="">${data.resultSet[i].numUnd}</td>
                        <td class="">${data.resultSet[i].nome}</td>
                        <td class="">${data.resultSet[i].nomeBloco}</td>
                        <td class="">${data.resultSet[i].metragem}</td>
                        <td class="">${data.resultSet[i].qtVagas}</td>
                        <td>${data.resultSet[i].dataCadastro}</td>
                        <td>
                            <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                            <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                        </td>
                    </tr>
                    `;
                    }
                }
                $('.loader').remove();
                $('#renderMorador').html(estrutura);
            }
        })
    }
    renderizaMorador();

    $(document).on('click','.excluir', function()
    {
        link = $(this);
        idCadastro = $(this).attr('data-id');
        dados = `id=${idCadastro}`;
        postApi('http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/delete', dados);
    })

})
