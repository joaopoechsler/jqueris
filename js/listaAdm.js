$(function () {

    function renderizaAdm() {
        $('#renderAdm').html('');
        estrutura = `
        <tr>
            <td>Id</td>
            <td>Nome</td>
            <td>CNPJ</td>
            <td>Data Cadastro</td>
            <td>&nbsp;</td>
        </tr>
        `;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/get-all',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.resultSet) {
                    for (let i = 0; i < data.resultSet.length; i++) {
                        estrutura += `
                    <tr data-id="${data.resultSet[i].id}">
                        <td>${data.resultSet[i].id}</td>
                        <td class="nomeAdm">${data.resultSet[i].nomeAdm}</td>
                        <td class="cnpj">${data.resultSet[i].cnpj}</td>
                        <td>${data.resultSet[i].dataCadastro}</td>
                        <td>
                            <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                            <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                        </td>
                    </tr>
                    `;
                    }
                }
                $('.loader').remove();
                $('#renderAdm').html(estrutura);
            }
        })
    }
    renderizaAdm();

    $(document).on('click', 'a[href="#"].editar', function () {
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/get-one&id=' + idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                nomeAdm = data.resultSet[0].nomeAdm;
                $(link).parent().parent().find('.nomeAdm').html(`<input class="form-control bg-dark text-light" name="nomeAdm" value="${data.resultSet[0].nomeAdm}">`);
                cnpj = data.resultSet[0].cnpj;
                $(link).parent().parent().find('.cnpj').html(`<input class="form-control bg-dark text-light" name="cnpj" value="${data.resultSet[0].cnpj}">`);
            }
        })

        //criar botões
        $(link).toggleClass(['editar', 'salvar']);
        $(link).html('Salvar');

        $(link).parent().find('.excluir').html('Cancelar');
        $(link).parent().find('.excluir').toggleClass(['excluir', 'cancelar']);

        $('.editar, .excluir').removeAttr('href');
    })

    //cancelar edição
    $(document).on('click', '.cancelar', function () {
        $(this).parent().parent().find('.nomeAdm').html(nomeAdm);
        $(this).parent().parent().find('.cnpj').html(cnpj);

        //criar botões
        $(this).parent().find('.salvar').html('Editar');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);

        $(this).parent().find('.cancelar').html('Excluir');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'excluir']);

        $('.editar, .excluir').attr('href', '#');

    })

    //editar o registro de fato. Bora negada 
    $(document).on('click', '.salvar', function () {

        id = $(this).attr('data-id');
        nomeAdm = $('input[name="nomeAdm"]').val();
        cnpj = $('input[name="cnpj"]').val();


        dados = `nomeAdm=${nomeAdm}&cnpj=${cnpj}&id=${id}`;

        if (nomeAdm && cnpj) {
            retorno = postApi('http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/edit-adm', dados);
        }

    })

    $(document).on('click','.excluir', function()
    {
        link = $(this);
        idCadastro = $(this).attr('data-id');
        dados = `id=${idCadastro}`;
        postApi('http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/delete', dados);
    })

})
