$(function () {

    function renderizaCond() {
        $('#renderCond').html('');
        estrutura = `
            <tr>
            <td>Id</td>
            <td>Nome</td>
            <td>Blocos</td>
            <td>Rua</td>
            <td>N°</td>
            <td>Bairro</td>
            <td>Cidade</td>
            <td>Estado</td>
            <td>Cep</td>
            <td>Data Cadastro</td>
            <td>Data Att</td>
            </tr>
        `;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/get-all',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.resultSet) {
                    for (let i = 0; i < data.resultSet.length; i++) {
                        estrutura += `
                    <tr data-id="${data.resultSet[i].id}">
                        <td>${data.resultSet[i].id}</td>
                        <td class="nomeAdm">${data.resultSet[i].nome}</td>
                        <td class="">${data.resultSet[i].qtUnidadesAndar}</td>
                        <td class="">${data.resultSet[i].rua}</td>
                        <td class="">${data.resultSet[i].num}</td>
                        <td class="">${data.resultSet[i].bairro}</td>
                        <td class="">${data.resultSet[i].cidade}</td>
                        <td class="">${data.resultSet[i].estado}</td>
                        <td class="">${data.resultSet[i].cep}</td>
                        <td>${data.resultSet[i].dataCadastro}</td>
                        <td>
                            <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                            <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                        </td>
                    </tr>
                    `;
                    }
                }
                $('.loader').remove();
                $('#renderCond').html(estrutura);
            }
        })
    }
    renderizaCond();

    $(document).on('click','.excluir', function()
    {
        link = $(this);
        idCadastro = $(this).attr('data-id');
        dados = `id=${idCadastro}`;
        postApi('http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/delete', dados);
    })

})
