$(function () {

    $('form').submit(function () {
        url = $(this).attr('action');
        data = $(this).serialize();
        postApi(url, data);
        return false;
    })

    postApi = function(url, dados, reload) {
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/default/get-token-post',
            method: 'GET',
            dataType: 'json',
            success: function (tk) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: dados + '&_csrf=' + tk.token,
                    success: function (data) {
                        if (data.endPoint.status == 'success') {
                            location.reload()
                        }
                    }
                })
            }
        })

    }

    loadAdm = function(){
        options = `<option value="">Selecione...</option>`;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nomeAdm}</option>`;
                }
                $('.fromAdministradora').html(options)
            }
        })

    }

    loadCondo = function(){
        let options = `<option value="">Selecione o Condomínio</option>`;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nome}</option>`;
                }
                $('.fromCond').html(options)
            }
        })

    }

    $('.fromCond').ready(function(){
        loadCondo();
    })

    $('.fromAdministradora').ready(function(){
        loadAdm();
    })

    $('.fromAdministradora').change(function(){
        let options = `<option value="">Selecione a Administradora</option>`;
        let id = $(this).val();
        $.ajax({
            url: `http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/get-condominio-from-adm&from_administradora=${id}`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nome}</option>`;
                }
                $('.fromCond').html(options)
            }
        })
    })

    $('.fromCond').change(function(){
        let options = `<option value="">Selecione o Bloco</option>`;
        let id = $(this).val();
        $.ajax({
            url: `http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/unidades/get-condominio-from-bloco&from_bloco=${id}`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nomeBloco}</option>`;
                }
                $('.fromBloco').html(options)
            }
        })
    })

    $('.fromEstado').change(function(){
        let estado = $(this).val();
        let estadoSelecionado = $(this).find(`option[value="${estado}"]`).attr('data-uf');
        let options = `<option value="">Selecione...</option>`;
        $.ajax({
            url: `http://servicodados.ibge.gov.br/api/v1/localidades/estados/${estadoSelecionado}/municipios`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                for (const key in data) {
                    options += `<option value="${data[key].nome}">${data[key].nome}</option>`;
                }
                $('.cidades').html(options);
            }

        })
    })

})

// http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/administradoras/edit-adm