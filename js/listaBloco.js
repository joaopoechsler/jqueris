$(function () {

    function renderizaBloco() {
        $('#renderBloco').html('');
        estrutura = `
            <tr>
            <td>Nome</td>
            <td>Nome Bloco</td>
            <td>Qtd Unidades</td>
            <td>Andares</td>
            <td>Data Cadastro</td>
            </tr>
        `;
        $.ajax({
            url: 'http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/blocos/get-all',
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.resultSet) {
                    for (let i = 0; i < data.resultSet.length; i++) {
                        estrutura += `
                    <tr data-id="${data.resultSet[i].id}">
                        <td class="">${data.resultSet[i].nome}</td>
                        <td class="">${data.resultSet[i].nomeBloco}</td>
                        <td class="">${data.resultSet[i].qtUnidadesAndar}</td>
                        <td class="">${data.resultSet[i].Andares}</td>
                        <td>${data.resultSet[i].dataCadastro}</td>
                        <td>
                            <a href="#" data-id="${data.resultSet[i].id}" class="editar">Editar</a>
                            <a href="#" data-id="${data.resultSet[i].id}" class="excluir">Deletar</a>
                        </td>
                    </tr>
                    `;
                    }
                }
                $('.loader').remove();
                $('#renderBloco').html(estrutura);
            }
        })
    }
    renderizaBloco();

    $(document).on('click','.excluir', function()
    {
        link = $(this);
        idCadastro = $(this).attr('data-id');
        dados = `id=${idCadastro}`;
        postApi('http://localhost/FrameworkYii/meuprimeiroyii/web/index.php?r=api/condominios/delete', dados);
    })

})
