<?
error_reporting(0);
include "vars.php"
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Jqueris</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="?page=inicio"></a><img style="width: 70px; height: auto;" src="https://images.gamebanana.com/img/ico/sprays/61118a0b23ab7.gif" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administradoras
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=listaAdm">Listar Adm</a>
                        <a class="dropdown-item" href="?page=cadAdm">Cadastrar Adm</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Condominios
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=listaCond">Listar Condominios</a>
                        <a class="dropdown-item" href="?page=cadCond">Cadastrar Condominios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Blocos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=listaBloco">Listar Blocos</a>
                        <a class="dropdown-item" href="?page=cadBloco">Cadastrar Blocos</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Unidades
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=listaUnidade">Listar Unidade</a>
                        <a class="dropdown-item" href="?page=cadUnidade">Cadastrar Unidade</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Moradores
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=listaMorador">Listar Adm</a>
                        <a class="dropdown-item" href="?page=cadMorador">Cadastrar Adm</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container mt-5">

        <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "views/inicio.php";
                break;
            default:
                require 'views/' . $_GET['page'] . '.php';
                break;
        }
        ?>

        <div class="row justify-content-center">
            <center>
                <div class="col col-12 text-center">
                    <!-- <img class="text-center col-12 mt-1" src="https://images.gamebanana.com/img/ico/sprays/61118a0b23ab7.gif" alt=""> -->
                </div>
            </center>
        </div>
    </main>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/app.js"></script>
    <script src="js/<?= $_GET['page'] ?>.js"></script>
</body>

</html>